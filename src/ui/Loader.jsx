import React from 'react';

import loadingPicURL from '../assets/img/loader.gif';

export default function Loader() {
  return <img src={loadingPicURL} width="25px" alt="анимация загрузки" />;
}
