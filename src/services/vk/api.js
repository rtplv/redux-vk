export default (method, params) =>
  new Promise((resolve, reject) => {
    try {
      //eslint-disable-next-line no-undef
      VK.Api.call(method, { v: 5.87, ...params }, r => resolve(r.response));
    } catch (err) {
      reject(err);
    }
  });
