import api from './api';

export const getAllPhotos = () => {
  const params = {
    owner_id: 37027239,
    skip_hidden: true,
    extended: true,
  };
  return api('photos.getAll', params);
};
