import { combineReducers } from 'redux';

import pageReducer from './containers/Page/reducer';
import userReducer from './containers/User/reducer';

export default combineReducers({
  page: pageReducer,
  user: userReducer,
});
