import { createStore, compose, applyMiddleware } from 'redux';
import rootReducer from '../rootReducer';

// import customLogger from './middlewares/logger';
import logger from 'redux-logger';
import thunk from 'redux-thunk';

const middlewares = applyMiddleware(thunk, logger);

export const store = createStore(
  rootReducer,
  compose(
    middlewares,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  )
);
