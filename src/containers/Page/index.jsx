import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { setYear, getPhotos } from './actions';
import { photosByYearSelector } from './selectors';

import Loader from '../../ui/Loader';

import './Page.css';

class Page extends Component {
  static propTypes = {
    year: PropTypes.number.isRequired,
    photos: PropTypes.object.isRequired,
    setYear: PropTypes.func.isRequired,
    getPhotos: PropTypes.func.isRequired,
    isFetching: PropTypes.bool.isRequired,
  };

  selectYear = year => {
    const { isFetching, photos, setYear, getPhotos } = this.props;

    if (isFetching) return;

    setYear(year);

    if (!photos.items) {
      getPhotos();
    }
  };

  renderYearPickerList = () => {
    const years = [2014, 2015, 2016, 2017, 2018];
    return (
      <ul className="year-picker__list">
        {years.map(year => (
          <li key={year}>
            <button className="btn" onClick={() => this.selectYear(year)}>
              {year}
            </button>
          </li>
        ))}
      </ul>
    );
  };

  renderPhotosList = () => {
    const {
      photos: { items },
    } = this.props;

    if (items && items.length) {
      return (
        <ul className="photos-list">
          {items.map(photo => (
            <li className="photos-list__item" key={photo.id}>
              <figure className="photo">
                <img src={photo.sizes[3].url} alt={photo.text} />
                <figcaption>{photo.text}</figcaption>
              </figure>
              <div className="photo-addition__bar">
                <span className="like-btn">
                  <span className="like-btn__icon" />
                  <span className="like-btn__count">{photo.likes.count}</span>
                </span>
              </div>
            </li>
          ))}
        </ul>
      );
    }
    return <span className="photo__empty-message">Данные отсутствуют</span>;
  };

  render() {
    const { isFetching } = this.props;
    return (
      <>
        <header className="Page__header">
          <h3>Топ фото</h3>
        </header>
        <section className="Page">
          <article className="Page__year-picker">
            {this.renderYearPickerList()}
          </article>
          <article className="Page__photos">
            {isFetching ? <Loader /> : this.renderPhotosList()}
          </article>
        </section>
      </>
    );
  }
}

export default connect(
  state => ({
    year: state.page.year,
    photos: photosByYearSelector(state),
    isFetching: state.page.isFetching,
  }),
  { setYear, getPhotos }
)(Page);
