import {
  SET_YEAR,
  GET_PHOTOS_REQUEST,
  GET_PHOTOS_SUCCESS,
} from './actionTypes';
import { getAllPhotos } from '../../services/vk/PhotosService';

export const setYear = year => ({
  type: SET_YEAR,
  payload: year,
});

export const getPhotos = () => async dispatch => {
  dispatch({
    type: GET_PHOTOS_REQUEST,
  });

  try {
    const response = await getAllPhotos();
    dispatch({
      type: GET_PHOTOS_SUCCESS,
      payload: response,
    });
  } catch (err) {
    console.error(err);
  }
};
