import {
  SET_YEAR,
  GET_PHOTOS_REQUEST,
  GET_PHOTOS_SUCCESS,
} from './actionTypes';

const initialState = {
  year: 2018,
  photos: {},
  isFetching: false,
};

export default (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case SET_YEAR:
      return {
        ...state,
        year: payload,
      };
    case GET_PHOTOS_REQUEST:
      return {
        ...state,
        isFetching: true,
      };
    case GET_PHOTOS_SUCCESS:
      return {
        ...state,
        photos: payload,
        isFetching: false,
      };
    default:
      return state;
  }
};
