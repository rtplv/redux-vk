export const SET_YEAR = 'page/SET_YEAR';
export const GET_PHOTOS_REQUEST = 'page/GET_PHOTOS_REQUEST';
export const GET_PHOTOS_SUCCESS = 'page/GET_PHOTOS_SUCCESS';
export const GET_PHOTOS_ERROR = 'page/GET_PHOTOS_ERROR';
