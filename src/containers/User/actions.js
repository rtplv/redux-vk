import { LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAIL } from './actionTypes';

export const handleLogin = () => dispatch => {
  dispatch({
    type: LOGIN_REQUEST,
  });
  //eslint-disable-next-line no-undef
  VK.Auth.login(r => {
    if (r.session) {
      dispatch({
        type: LOGIN_SUCCESS,
        payload: r.session,
      });
    } else {
      dispatch({
        type: LOGIN_FAIL,
        error: true,
        payload: new Error('Ошибка авторизации'),
      });
    }
  }, 4);
};
