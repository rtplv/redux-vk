import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { handleLogin } from './actions';

import './User.css';

import Loader from '../../ui/Loader';

export class User extends Component {
  static propTypes = {
    session: PropTypes.object,
    isFetching: PropTypes.bool,
    error: PropTypes.string,
    handleLogin: PropTypes.func,
  };

  renderUserBlock = () => {
    const { error, session, isFetching, handleLogin } = this.props;

    if (isFetching) {
      return <Loader />;
    }

    if (error) {
      return (
        <div className="User__error">
          <span className="User__error-message">{error}</span>
          <button className="btn" onClick={handleLogin}>
            Попробовать еще раз
          </button>
        </div>
      );
    }

    if (session && session.user) {
      const { first_name, last_name } = session.user;
      return (
        <div className="User__greeting">
          Привет, {first_name} {last_name}!
        </div>
      );
    } else {
      return (
        <button className="btn" onClick={handleLogin}>
          Войти
        </button>
      );
    }
  };
  render() {
    return <div className="User">{this.renderUserBlock()}</div>;
  }
}

export default connect(
  state => ({
    session: state.user.session,
    isFetching: state.user.isFetching,
    error: state.user.error,
  }),
  { handleLogin }
)(User);
