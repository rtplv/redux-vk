import { LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAIL } from '../User/actionTypes';

const initialState = {
  session: null,
  isFetching: false,
  error: '',
};

export default (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case LOGIN_REQUEST:
      return {
        ...state,
        isFetching: true,
        error: '',
      };
    case LOGIN_SUCCESS:
      return {
        ...state,
        isFetching: false,
        session: payload,
      };
    case LOGIN_FAIL:
      return {
        ...state,
        isFetching: false,
        error: payload.message,
      };
    default:
      return state;
  }
};
