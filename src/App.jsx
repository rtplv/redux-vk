import React, { Component } from 'react';

import User from './containers/User';
import Page from './containers/Page';

import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <User />
        <Page />
      </div>
    );
  }
}

export default App;
